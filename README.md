# assessment_1

# Setting up a CI/CD Pipeline with GitLab

Welcome to the GitLab repository for the assessment "Setting up a CI/CD Pipeline with GitLab". In this assignment, I've established a Continuous Integration/Continuous Deployment (CI/CD) pipeline using GitLab CI/CD and Jenkins to automate the deployment process of a simple Flask web application - a basic login form.

## Prerequisites

Before setting up the CI/CD pipeline and running the Flask application, ensure you have the following installed:

- **Python**: Make sure you have Python installed on your system. You can download it from [python.org](https://www.python.org/downloads/).
- **Git**: Install Git to clone the repository and manage version control. You can download it from [git-scm.com](https://git-scm.com/downloads).
- **Flask**: Install Flask, a lightweight WSGI web application framework. You can install it using `pip install Flask`.
- **Jenkins** : Install jenkins to deploy in jenkins server

## Project Overview

This project consists of the following components:

1. **GitLab Repository**: A public repository created for this assignment to host the Flask application code and the CI/CD pipeline configuration.

2. **Flask Web Application**: A simple Flask web application featuring a login form. The application is built using Python and Flask framework.

3. **.gitlab-ci.yml**: Configuration file defining the CI/CD pipeline stages and jobs for building, testing, and deploying the application.

## Project Structure

1. app.py # Main Flask application file
2. requirements.txt # List of Python dependencies
3. .gitlab-ci.yml # CI/CD pipeline configuration file
4. README.md # Documentation file (you're currently reading it)

The `.gitlab-ci.yml` file defines the stages and jobs for the CI/CD pipeline. 

The stages were used to stage the process. The stages included were:
  1. build 
  2. test
  3. deploy

This configuration sets up three stages: build, test, and deploy, each containing respective jobs. The jobs include setting up the environment, building the application, testing, and deploying it to a staging environment.

## Running the Application
To run the Flask application locally:

1. Ensure you have Python and Flask installed on your system.
2. Clone this repository.
3. Install dependencies using pip install -r requirements.txt.
4. Set the Flask environment variables (FLASK_APP and FLASK_ENV) and activate the virtual environment.
5. Run the Flask application using flask run

## Troubleshooting Tips
1. Virtual Environment Activation: Ensure that you've activated the virtual environment before running the Flask application. If you encounter issues, double-check that the virtual environment is activated, especially if you see module import errors.
2. Dependency Installation: If you encounter dependency installation issues, make sure you have the correct permissions to install packages. Running the installation command with administrator or superuser privileges might resolve the problem.
3. Environment Variables: Check that the Flask environment variables (FLASK_APP and FLASK_ENV) are correctly set. If the application fails to run, ensure these variables are properly configured.


```
cd existing_repo
git remote add origin https://gitlab.com/Shivapriya26/assessment_1.git
git branch -M main
git push -uf origin main
```

